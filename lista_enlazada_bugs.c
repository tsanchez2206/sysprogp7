#include <stdio.h>
#include <stdlib.h>

typedef struct nodo {
	int numero;
	struct nodo* siguiente;
} nodo;

nodo *cabecera = NULL; 
nodo *actual = NULL;

void verLista()
{
	nodo *ptr;
	ptr = cabecera;

	printf("\n[Cabeza] =>");

	while(ptr != NULL) {        
		printf(" %d =>",ptr->numero);
		ptr = ptr->siguiente;
	}

	printf(" [null]\n");
}


int main (int argc, char *argv[])
{
	nodo *enlace, *temp;
	int num;

	while (1){
		printf("Ingrese un numero o escriba 0 para salir: ");
		scanf("%d",&num);
		temp = cabecera;
		
		if (num == 0)
			break;
		else{
			enlace = (nodo *) malloc(sizeof(nodo));
			enlace->numero = num;
			enlace->siguiente = temp;
			cabecera = enlace;
		}
	}
	verLista();
}
